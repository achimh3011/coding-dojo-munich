Python User Group Munich
========================

A collection of coding katas used at a coding dojo in Munich on Sep. 16, 2014.

Some hints:

* Please stick to PEP8.