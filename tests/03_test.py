#!/usr/bin/env python3

import pytest

from dojo import dojo_03


def test_prime_factors():
    numbers = [9, 17, 49, 1155]
    expected = [[3, 3], [17], [7, 7], [3, 7, 5, 11]]
    for number, exp in zip(numbers, expected):
        assert sorted(exp) == dojo_03.prime_factors(number)


if __name__ == '__main__':
    pytest.main()
