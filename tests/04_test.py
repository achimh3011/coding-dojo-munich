#!/usr/bin/env python3

import pytest

from dojo.dojo_04 import *


def print_result(who, desc):
    if desc:
        print(who, 'with', desc)
    else:
        print(who)


def test_high_ace_white():
    who, desc = compare_hands('2C 3H 4S 8C AH', '2H 3D 5S 9C KD')
    print_result(who, desc)
    assert who == 'White wins'
    assert desc == 'high card: Ace'


def test_full_house_black():
    who, desc = compare_hands('2S 8S AS QS 3S', '2H 4S 4C 2D 4H')
    print_result(who, desc)
    assert who == 'Black wins'
    assert desc == 'full house'


def test_high_card_black():
    who, desc = compare_hands('2C 3H 4S 8C KH', '2H 3D 5S 9C KD')
    print_result(who, desc)
    assert who == 'Black wins'
    assert desc == 'high card: 9'


def test_tie():
    who, desc = compare_hands('2D 3H 5C 9S KH', '2H 3D 5S 9C KD')
    print_result(who, desc)
    assert who == 'Tie'
    assert desc == ''


if __name__ == '__main__':
    pytest.main()
