#!/usr/bin/env python3

import pytest

from dojo.dojo_04 import *


def test_check_straight_flush():
    cards = convert_hand('TH 9H JH QH 8H')
    score, kickers, desc = check_straight_flush(cards)
    assert score == 9
    assert desc == 'straight flush'
    assert kickers == [NUMBERS.find('Q')]


def test_check_four():
    cards = convert_hand('QH QD QS QC 2D')
    score, kickers, desc = check_four(cards)
    assert score == 8
    assert desc == 'four'
    assert kickers == [NUMBERS.find('Q')]


def test_check_full_house():
    cards = convert_hand('TH TD TS 8C 8D')
    score, kickers, desc = check_full_house(cards)
    assert score == 7
    assert desc == 'full house'
    assert kickers == [NUMBERS.find('T')]


def test_check_flush():
    cards = convert_hand('7H 8H TH QH AH')
    score, kickers, desc = check_flush(cards)
    assert score == 6
    assert desc == 'flush'
    assert kickers == [NUMBERS.find('A')]


def test_check_straight():
    cards = convert_hand('7H 8H 9S TH JC')
    score, kickers, desc = check_straight(cards)
    assert score == 5
    assert desc == 'straight'
    assert kickers == [NUMBERS.find('J')]


def test_check_three():
    cards = convert_hand('7H 8H 8S TH 8C')
    score, kickers, desc = check_three(cards)
    assert score == 4
    assert desc == 'three'
    assert kickers == [NUMBERS.find('8')]


def test_check_two_pairs():
    cards = convert_hand('7H 8H 7S TS 8C')
    score, kickers, desc = check_two_pairs(cards)
    assert score == 3
    assert desc == 'two pairs'
    assert kickers == [NUMBERS.find('8')]


def test_check_pair():
    cards = convert_hand('7H 9H 7S TS 8C')
    score, kickers, desc = check_pair(cards)
    assert score == 2
    assert desc == 'pair'
    assert kickers == [NUMBERS.find('7'), NUMBERS.find('T')]


def test_high_card():
    cards = convert_hand('7H 9H 2S TS 8C')
    score, kickers, desc = check_high_card(cards)
    assert score == 1
    assert desc == 'high card'
    assert kickers == [NUMBERS.find(c) for c in 'T9872']


def test_high_card2():
    cards = convert_hand('7H 9H 2S JS 8C')
    score, kickers, desc = check_high_card(cards)
    assert score == 1
    assert desc == 'high card'
    assert kickers == [NUMBERS.find(c) for c in 'J9872']


def test_high_card3():
    cards = convert_hand('7H 9H 2S 4S 8C')
    score, kickers, desc = check_high_card(cards)
    assert score == 1
    assert desc == 'high card'
    assert kickers == [NUMBERS.find(c) for c in '98742']


if __name__ == '__main__':
    pytest.main()
