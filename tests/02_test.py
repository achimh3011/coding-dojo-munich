#!/usr/bin/env python3

import pytest

from dojo import dojo_02


def test_simple_verify_luhn():
    assert dojo_02.verify_luhn_checksum('79927398713') is True


def test_simple_calculate_luhn():
    assert dojo_02.calculate_luhn_checksum('7992739871') == '3'


def test_simple_calculate_luhn_short():
    assert dojo_02.calculate_luhn_checksum('1234') == '4'


def test_simple_verify_luhn_short():
    assert dojo_02.verify_luhn_checksum('12344') is True


if __name__ == '__main__':
    pytest.main()
