#!/usr/bin/env python3

import pytest

from dojo import dojo_01


def test_first():
    assert dojo_01.fizz_buzz(1) == '1'


def test_first_fizz():
    assert dojo_01.fizz_buzz(3) == 'fizz'


def test_first_buzz():
    assert dojo_01.fizz_buzz(5) == 'buzz'


def test_fizz_buzz():
    assert dojo_01.fizz_buzz(15) == 'fizz buzz'


def test_some_more():
    assert [dojo_01.fizz_buzz(i) for i in (12, 43, 34543, 22, 90)] == ['fizz', '43', '34543', '22', 'fizz buzz']


def test_all():
    from hashlib import sha1

    h = sha1(''.join(dojo_01.fizz_buzz(i) for i in range(1000)).encode())
    assert h.hexdigest() == '38c016cb28024868b4c052ec5c7c34a6d6e72447'


if __name__ == '__main__':
    pytest.main()
