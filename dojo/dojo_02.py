#!/usr/bin/env python3

""" Calculate / verify the Luhn checksum digit
    ==========================================

    The Luhn algorithm is for calculation / verification of a single-digit checksum digit for a row of numbers.
    It's not a very sophisticated algorithm and only guards against mistypings at most, but it is widely used.

    Calculation
    ###########

    Starting from the rightmost digit, moving left, double the value of every second digit;
    if the product of this doubling operation is greater than 9 (e.g., 7 × 2 = 14), then sum the digits of the
    products (e.g., 10: 1 + 0 = 1, 14: 1 + 4 = 5).
    The check digit (x) is obtained by computing the sum of digits then multiply by 9 take modulo 10 of the product.

    Verification
    ############

    From the rightmost digit, which is the check digit, moving left, double the value of every second digit;
    if the product of this doubling operation is greater than 9 (e.g., 7 × 2 = 14), then sum the digits of the
    products (e.g., 10: 1 + 0 = 1, 14: 1 + 4 = 5).
    Take the sum of all the digits.
    If the total modulo 10 is equal to 0 then the number is valid according to the Luhn formula; else it is not valid.

    .. _Luhn_algorithm: http://en.wikipedia.org/wiki/Luhn_algorithm

"""


def calculate_luhn_checksum(digits):
    return '0'


def verify_luhn_checksum(digits):
    return False
