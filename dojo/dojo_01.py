#!/usr/bin/env python3

""" fizz_buzz
    =========

    Write a function that returns the given number but for every number that is divisible by three return 'fizz'
    for every number divisible by 5 return 'fuzz'. If both are valid return 'fizz buzz'. Otherwise return the number.

    Extra points for using only one expression.

"""


def fizz_buzz(n):
    return 'ni'


if __name__ == '__main__':
    print(", ".join(fizz_buzz(i) for i in range(1, 101)))