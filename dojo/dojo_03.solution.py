#!/usr/bin/env python3

""" Prime Factors
    =============

    For a given number, return a list of its prime factors.

    http://butunclebob.com/ArticleS.UncleBob.ThePrimeFactorsKata
"""


def prime_factors_iterative(number, start=2):
    res = []
    start = 2
    while number > 1:
        for i in range(start, number + 1):
            if number % i == 0:
                res.append(i)
                number = int(number / i)
                start = i
    return res


def prime_factors_recursively(number, start=2):
    for i in range(start, number + 1):
        if number % i == 0:
            return [i] + prime_factors(int(number / i), i)
    return []


# Solution from Stefan Behnel
def iter_primes(n):
    primes = list(range(2, n + 1))
    for i in primes:
        if i is None:
            continue
        yield i
        k = i - 2
        while k < len(primes):
            primes[k] = None
            k += i


def prime_factors_with_iter(number):
    prime_fac = []
    for prime in iter_primes(number):
        while number % prime == 0:
            prime_fac.append(prime)
            number /= prime
    return prime_fac


prime_factors = prime_factors_iterative