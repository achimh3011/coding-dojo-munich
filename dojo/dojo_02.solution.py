#!/usr/bin/env python3

""" Calculate / verify the Luhn checksum digit
    ==========================================

    The Luhn algorithm is for calculation / verification of a single-digit checksum digit for a row of numbers.
    It's not a very sophisticated algorithm and only guards against mistypings at most, but it is widely used.

    Calculation
    ###########

    Starting from the rightmost digit, moving left, double the value of every second digit;
    if the product of this doubling operation is greater than 9 (e.g., 7 × 2 = 14), then sum the digits of the
    products (e.g., 10: 1 + 0 = 1, 14: 1 + 4 = 5).
    The check digit (x) is obtained by computing the sum of digits then computing 9 times that value modulo 10.

    Verification
    ############

    From the rightmost digit, which is the check digit, moving left, double the value of every second digit;
    if the product of this doubling operation is greater than 9 (e.g., 7 × 2 = 14), then sum the digits of the
    products (e.g., 10: 1 + 0 = 1, 14: 1 + 4 = 5).
    Take the sum of all the digits.
    If the total modulo 10 is equal to 0 then the number is valid according to the Luhn formula; else it is not valid.

    .. _Luhn_algorithm: http://en.wikipedia.org/wiki/Luhn_algorithm

"""


def _process_digits(digits):
    second = (d * 2 if d < 5 else d * 2 - 9 for d in (int(dd) for dd in digits[::-2]))
    other = (int(d) for d in digits[-2::-2])
    return sum(other) + sum(second)


def calculate_luhn_checksum(digits):
    return chr(48 + (9 * _process_digits(digits)) % 10)


def verify_luhn_checksum(digits):
    check_digit = int(digits[-1])
    normal_digits = digits[:-1]
    res = (check_digit + _process_digits(normal_digits)) % 10
    return res == 0