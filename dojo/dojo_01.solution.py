#!/usr/bin/env python3

""" fizz_buzz
    =========

    Write a function that returns the given number but for every number that is divisible by three return 'fizz'
    for every number divisible by 5 return 'fuzz'. If both are valid return 'fizz buzz'. Otherwise return the number.

    Extra points for using only one expression.

"""


def fizz_buzz_long(n):
    res = []
    if n % 3 == 0:
        res.append('fizz')
    if n % 5 == 0:
        res.append('buzz')
    if res:
        return ' '.join(res)
    return '%d' % n


def fizz_buzz_short(n):
    return {(True, False): 'fizz', (False, True): 'buzz', (True, True): 'fizz buzz'}.get(((n % 3 == 0), (n % 5 == 0)),
                                                                                         '%d' % n)


fizz_buzz = fizz_buzz_long


if __name__ == '__main__':
    print(", ".join(fizz_buzz(i) for i in range(1, 101)))