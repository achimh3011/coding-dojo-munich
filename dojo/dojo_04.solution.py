#!/usr/bin/env python3

""" Poker Hands
    ===========

    Write a function that compares two poker hands.

    A poker deck contains 52 cards - each card has a suit which is one of clubs, diamonds, hearts, or spades (denoted
    C, D, H, and S in the input data). Each card also has a value which is one of 2, 3, 4, 5, 6, 7, 8, 9, 10, jack,
    queen, king, ace (denoted 2, 3, 4, 5, 6, 7, 8, 9, T, J, Q, K, A). For scoring purposes, the suits are unordered
    while the values are ordered as given above, with 2 being the lowest and ace the highest value.

    A poker hand consists of 5 cards dealt from the deck. Poker hands are ranked by the following partial order from
    lowest to highest.

    * High Card: Hands which do not fit any higher category are ranked by the value of their highest card. If the
      highest cards have the same value, the hands are ranked by the next highest, and so on.

    * Pair: 2 of the 5 cards in the hand have the same value. Hands which both contain a pair are ranked by the value
      of the cards forming the pair. If these values are the same, the hands are ranked by the values of the cards not
      forming the pair, in decreasing order.

    * Two Pairs: The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of
      their highest pair. Hands with the same highest pair are ranked by the value of their other pair. If these values
      are the same the hands are ranked by the value of the remaining card.

    * Three of a Kind: Three of the cards in the hand have the same value. Hands which both contain three of a kind
      are ranked by the value of the 3 cards.

    * Straight: Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by
      their highest card.

    * Flush: Hand contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High
      Card.

    * Full House: 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3
      cards.

    * Four of a kind: 4 cards with the same value. Ranked by the value of the 4 cards.

    * Straight flush: 5 cards of the same suit with consecutive values. Ranked by the highest card in the hand.

    http://codingdojo.org/cgi-bin/index.pl?KataPokerHands
"""
from collections import Counter
from functools import total_ordering

from operator import attrgetter

COLORS = 'CDHS'
NUMBERS = '23456789TJQKA'
NUMBERS_LONG = list('23456789') + ['10', 'Jack', 'Queen', 'King', 'Ace']


@total_ordering
class Card:
    def __init__(self, s):
        errors = []
        if len(s) != 2:
            errors.append('Each card is defined by two letters only.')
        number = NUMBERS.find(s[0])
        if number == -1:
            errors.append('Number %s unknown.' % s[0])
        color = COLORS.find(s[1])
        if color == -1:
            errors.append('Color %s unknown.' % s[1])
        if len(errors) > 0:
            raise ValueError(' '.join(errors) + ' Card was "%s"' % s)
        self.color, self.number = color, number

    def __str__(self):
        return NUMBERS[self.number] + COLORS[self.color]

    def __hash__(self):
        return hash((self.number, self.color))

    # isn't there a way  to define a __key__method ?
    def _is_valid_operand(self, other):
        return (hasattr(other, "color") and
                hasattr(other, "number"))

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented  # problematic before 3.4
        return (self.number, self.color) == (other.number, other.color)

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented  # problematic before 3.4
        return (self.number, self.color) < (other.number, other.color)

def has_card(hand, number):
    return any(card for card in hand if card.number == number)


def check_straight_flush(cards):
    ref_num = cards[0].number
    ref_col = cards[0].color
    if not all(c.color == ref_col for c in cards):
        return
    for i, n in enumerate(cards[1:]):
        if n.number != ref_num - 1 - i:
            return
    return 9, [ref_num], 'straight flush'


def check_four(cards):
    counter = Counter(c.number for c in cards)
    number, count = counter.most_common(1)[0]
    if count == 4:
        return 8, [number], 'four'


def check_full_house(cards):
    counter = Counter(c.number for c in cards)
    values = counter.most_common(2)
    if values[0][1] == 3 and values[1][1] == 2:
        number = values[0][0]
        return 7, [number], 'full house'


def check_flush(cards):
    counter = Counter(c.color for c in cards)
    dummy, count = counter.most_common(1)[0]
    if count == 5:
        high = max(c.number for c in cards)
        return 6, [high], 'flush'


def check_straight(cards):
    ref_num = cards[0].number
    ref_col = cards[0].color
    for i, n in enumerate(cards[1:]):
        if n.number != ref_num - 1 - i:
            return
    return 5, [ref_num], 'straight'


def check_three(cards):
    counter = Counter(c.number for c in cards)
    number, count = counter.most_common(1)[0]
    if count == 3:
        return 4, [number], 'three'


def check_two_pairs(cards):
    counter = Counter(c.number for c in cards)
    values = counter.most_common(2)
    if values[0][1] == 2 and values[1][1] == 2:
        number = max(values[0][0], values[1][0])
        return 3, [number], 'two pairs'


def check_pair(cards):
    counter = Counter(c.number for c in cards)
    number, count = counter.most_common(1)[0]
    if count == 2:
        highest_other = max(c.number for c in cards if c.number != number)
        return 2, [number, highest_other], 'pair'


def check_high_card(cards):
    score = 0
    for c in cards:
        score = 20 * score + c.number
    return 1, [c.number for c in cards], 'high card'


def convert_hand(hand):
    cards = set(Card(c) for c in hand.split())
    if len(cards) != 5:
        raise ValueError("Five distinct cards are needed, not %d" % len(cards))
    return list(sorted(cards, reverse=True))


def score_hand(hand):
    cards = convert_hand(hand)
    for check in [check_straight_flush, check_four, check_full_house, check_flush, check_straight,
                  check_three, check_two_pairs, check_pair, check_high_card]:
        result = check(cards)
        if result is not None:
            return result


def compare_hands(white, black):
    score_white, white_kickers, white_desc = score_hand(white)
    score_black, black_kickers, black_desc = score_hand(black)
    if score_white > score_black:
        return "White wins", white_desc
    elif score_white < score_black:
        return "Black wins", black_desc
    for white, black in zip(white_kickers, black_kickers):
        if white > black:
            return "White wins", white_desc + ': ' + NUMBERS_LONG[white]
        elif white < black:
            return "Black wins", black_desc + ': ' + NUMBERS_LONG[black]
    return 'Tie', ''
